/**
 * Created by Andre on 03.05.2017.
 */

var target = 0;
var target2 = 0;
var number = 0;
var clock = 1;
var playField = [];
var Ships = [];
var forbidden_targets = {
    up:[2,3,4,5,6,7,8,9],
    left:[11,21,31,41,51,61,71,81], // Nummern der Randfelder
    right:[20,30,40,50,60,70,80,90],
    down:[92,93,94,95,96,97,98,99],
    corner:[1,10,91,100]

            };

var allowed_targets = [ 12,13,14,15,16,17,18,19,22,
                        23,24,25,26,27,28,29,32,33,
                        34,35,36,37,38,39,42,43,44, // Nummern der Felder die nicht direkt am Rand liegen
                        45,46,47,48,49,52,53,54,55,
                        56,57,58,59,62,63,64,65,66,
                        67,68,69,72,73,74,75,76,77,
                        78,79,82,83,84,85,86,87,88,
                        89
                                    ];


function enemyPhase(playfield, ships) {

    playField=playfield;
    Ships=ships;
    reset();

    function reset() {  // Ausgangssituation  bis ein Schiff getroffen wird
        clock = 1;
        target = Math.floor((Math.random() * 100) + 1);
        target2 = 0;
        fire(target);
    }

    function new_target(number) { // Bestimmung eines neuen Ziels
       var field = [];


        for (var i = 1; i <= 100; i++) {
            // mögliche Ziele basiend auf dem vorherigen Treffer
            if (number === forbidden_targets.up[i] && clock < 3) {
                 field = [number + 10, number - 1, number + 1];
            }
            else if (number === forbidden_targets.left[i] && clock < 3) {
                 field = [number + 10, number + 1, number - 10];
            }
            else if (number === forbidden_targets.right[i] && clock < 3) {
                field = [number + 10, number - 1, number - 10];
            }
            else if (number === forbidden_targets.down[i] && clock < 3) {
                 field = [number + 1, number - 1, number - 10];
            }
            // Feststellung ob Schiff vertikal
            else if (Math.abs(target - target2 > 5 && clock > 2 && number === allowed_targets[i])) {
                 field = [number + 10, number - 10];
            }
            else {
                 field = [number - 1, number + 1];
            }
        }
        // Nachbarfelder der Eckpunkte
        if (number === forbidden_targets.corner[1] && clock < 3) {
            field = [number + 10, number + 1];
        }
        else if (number === forbidden_targets.corner[2] && clock < 3) {
           field = [number + 10, number + 1];
        }
        else if (number === forbidden_targets.corner[3] && clock < 3) {
            field = [number + 1, number - 10];
        }
        else if (number === forbidden_targets.corner[4] && clock < 3) {
              field = [number - 1, number - 10];
        }
//zufällige Ermittlung des nächsten Ziels
    return target2 = field[Math.round(Math.random() * field.length-0,5)];

    }

    function fire(number) { // Angriff

        if (playField[number].type === 1 && playField[number].hit !== true) {
            //Verhalten wenn Schiff auf dem Feld
            if(Ships[playField[number].Shipid].hp === 1) {
                bothit(number);
                reset();
            }else{
                bothit(number);
                target2 = new_target(number);
                clock++;
                fire(target2);
            }
        }

        else if (playField[number].type === 0  && playField[number].hit !== true) {
            //Verhalten wenn kein Schiff auf dem Feld
            bothit(number);
        }
        else if (playField[number].hit === true) {
            //Verhalten wenn Feld bereits zuvor Angegriffen
                fire(new_target(number));
            }
        }


        }




